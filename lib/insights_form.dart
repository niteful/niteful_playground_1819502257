import 'dart:ui';
import 'package:flutter/material.dart';
import 'resources/storage.dart';
import 'insights.dart';

class InsightsForm extends StatefulWidget {
  final Storage storage = Storage();

  @override
  _InsightsFormState createState() => _InsightsFormState();
}

class _InsightsFormState extends State<InsightsForm> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Padding(
          padding: EdgeInsets.all(30),
          child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Container(
              height: 10
            ),
            new Text("This is the insight you react to.", textAlign: TextAlign.center, style: TextStyle(color: Color.fromRGBO(0, 13, 27, 42), fontSize: 20)),
            new Container(
              height: 10
            ),
            new Container(
              child: new TextField(
                style: new TextStyle(color: Color.fromRGBO(0, 13, 27, 42), fontSize: 20),
                autofocus: false,
                cursorColor: Color.fromRGBO(0, 13, 27, 42),
                keyboardType: TextInputType.multiline,
                maxLines: null,
              ),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 3.0, color: Color.fromRGBO(0, 13, 27, 42)),
                  ),
                ),
              )
            ] 
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {},
          backgroundColor: Color.fromRGBO(0, 13, 27, 42),
          child: new IconButton
          (
              onPressed: () {
                Navigator.push(
                    context, 
                    MaterialPageRoute(builder: (context) => InsightsPage()),
                );
              },
              icon: new Icon(Icons.arrow_forward, color: Colors.white),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      );
  }
}